FROM alpine:latest

RUN apk update && apk add ca-certificates

COPY bin/scratch /scratch
CMD ["/scratch"]
